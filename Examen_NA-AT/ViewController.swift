//
//  ViewController.swift
//  Examen_NA-AT
//
//  Created by Ramón Omar Reyes López on 16/04/18.
//  Copyright © 2018 Ramón Omar Reyes López. All rights reserved.
//
// https://maps.googleapis.com/maps/api/place/nearbysearch/json?location=19.409737,-99.169601&radius=2000&type=bank&key=AIzaSyA7m5YQp_O%20QXvZ7DzylErwubKq7BhIVUcs


import UIKit
import CoreLocation

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    let latitud = 19.409737
    let longitud = -99.169601
    var activityIndicator: UIActivityIndicatorView = UIActivityIndicatorView()
    let urlDatos = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?location="
    let urlDatos2 = "&radius=2000&type=bank&key=AIzaSyA7m5YQp_O%20QXvZ7DzylErwubKq7BhIVUcs"

    var indexArray = 0
    var lugares = [[String: Any]]()
    var dictionary = [[String: Any]] ()
    
    var posiciones = [Int]()
    @IBOutlet var tableViewLugares: UITableView!
    override func viewDidLoad() {
        
        tableViewLugares.delegate = self
        tableViewLugares.dataSource = self
        
        super.viewDidLoad()
        loadUrl()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

// MARK: CargarURL
    func loadUrl(){
        starActivity()
        guard let urlCargar = URL (string:"\(urlDatos)\(latitud),\(longitud)\(urlDatos2)") else { return }
        URLSession.shared.dataTask(with: urlCargar) { (data, response, error) in
            if error != nil{
                print(error as Any)
                return
            }
            guard let data = data else {return}

            do{
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as! [String:Any]

                for (llave, valor) in json{
                    if (llave == "results"){
                        if let arrayConTodo: [ [String:Any] ] = valor as? [ [String:Any] ]{
                            for dict in arrayConTodo{
                                let aux = dict
                                self.lugares.append(aux)
                            }
                        }
                    }

                }
                DispatchQueue.main.async { // Correct
                    self.tableViewLugares .reloadData()
                }
            }catch let jsonError as NSError{
                print("Error al obtener el json:",jsonError)
            }
        }.resume()
    }

    
// MARK: -TableView
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return lugares.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        stopActivity()
        let cell = tableViewLugares.dequeueReusableCell(withIdentifier: "cellPlaces") as! cellPlacesTableViewCell
        let aux = lugares[indexPath.row]

        for (llave, valor) in aux {
            if (llave == "name"){
                cell.nombreLugar.text = "\(valor)"
            }
            if (llave == "vicinity"){
                cell.direccionLugar.text = "\(valor)"
            }
            if (llave == "geometry"){
                let aux1 = valor as! [String: Any]
                for (llave1, valor1) in aux1 {
                    if (llave1 == "location"){
                        let aux2 = valor1 as! [String: Any]
                        var latitud1:Double = 0
                        var longitud1:Double = 0
                        
                        for (llave3, valor3) in aux2 {
                            if (llave3 == "lat"){
                                latitud1 = valor3 as! Double
                            }
                            if (llave3 == "lng"){
                                longitud1 = valor3 as! Double
                            }
                        }
                        
                        let myLocation = CLLocation(latitude: latitud, longitude: longitud)
                        let myBuddysLocation = CLLocation(latitude: latitud1, longitude: longitud1)
                        let distance = myLocation.distance(from: myBuddysLocation) / 1000
                        cell.distanciaLugar.text = "\(Double(round(1000*distance)/1000))"
                    }
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 150.0;//Choose your custom row height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        starActivity()
        indexArray = indexPath.row
        
        performSegue(withIdentifier: "detailPlace", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
       let detail = segue.destination as! detailViewController
        
        detail.lugares = [lugares[indexArray]]
        stopActivity()
    }
    // MARK: -ACTIVITY INDICATOR
    func starActivity(){
        activityIndicator.center = self.view.center
        activityIndicator.transform = CGAffineTransform(scaleX: 3.75, y: 3.75);
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = UIColor.black
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        UIApplication.shared.beginIgnoringInteractionEvents()
    }

    func stopActivity(){
        activityIndicator.stopAnimating()
        UIApplication.shared.endIgnoringInteractionEvents()
    }

    
}

