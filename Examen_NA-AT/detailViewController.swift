//
//  detailViewController.swift
//  Examen_NA-AT
//
//  Created by Ramón Omar Reyes López on 17/04/18.
//  Copyright © 2018 Ramón Omar Reyes López. All rights reserved.
//

import UIKit

class detailViewController: UIViewController {

    var lugares = [[String: Any]]()

    @IBOutlet var lblCoordenadas: UILabel!
    @IBOutlet var lblTipo: UILabel!
    @IBOutlet var lblRating: UILabel!
    @IBOutlet var lblDireccion: UILabel!
    @IBOutlet var lblNombre: UILabel!
    @IBOutlet var imgBanco: UIImageView!
    
    var imageUrlString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let aux = lugares[0]

        for (llave, valor) in aux {
            if (llave == "name"){
                self.lblNombre.text = "\(valor)"
            }else if (llave == "icon"){
                self.imageUrlString = "\(valor)"
                cargarImagen()
            }else if (llave == "vicinity"){
                self.lblDireccion.text = "\(valor)"
            }else if (llave == "rating"){
                self.lblRating.text = "\(valor)"
            }else if (llave == "types"){
                self.lblTipo.text = "Banco, Finanzas"
            }else if (llave == "geometry"){
                let aux1 = valor as! [String: Any]
                for (llave1, valor1) in aux1 {
                    if (llave1 == "location"){
                        let aux2 = valor1 as! [String: Any]
                        var latitud:Double = 0
                        var longitud:Double = 0
                        
                        for (llave3, valor3) in aux2 {
                            if (llave3 == "lat"){
                                latitud = valor3 as! Double
                            }
                            if (llave3 == "lng"){
                                longitud = valor3 as! Double
                            }
                        }
                        self.lblCoordenadas.text = "\(Double(round(1000*latitud)/1000))-- \(Double(round(1000*longitud)/1000))"
                    }
                }
            }
            
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnBack(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true);
    }
    
    func cargarImagen() {
        let imageUrl:URL = URL(string: imageUrlString)!
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            let imageData:NSData = NSData(contentsOf: imageUrl)!
            // When from background thread, UI needs to be updated on main_queue
            DispatchQueue.main.async {
                let image = UIImage(data: imageData as Data)
                self.imgBanco.image = image
                self.imgBanco.contentMode = UIViewContentMode.scaleAspectFit
                self.imgBanco.layer.borderWidth = 3
                self.imgBanco.layer.borderColor = UIColor.gray.cgColor
                self.imgBanco.layer.cornerRadius = 50
                self.imgBanco.clipsToBounds = true

            }
        }
    }

}
